## Cloudreve on Docker - 即开即用的私人网盘方案

## 0. 关于镜像
> 此镜像已经被官方钦定（大雾），[传送门 >>>](https://github.com/HFO4/Cloudreve/wiki/%E5%AE%89%E8%A3%85%E8%AF%B4%E6%98%8E#%E9%80%9A%E8%BF%87docker%E5%AE%89%E8%A3%85)  
> 详细部署教程，请移步：https://blog.ilemonrain.com/docker/cloudreve.html
>
> **注意：此镜像目前处于测试阶段，不建议大量部署到生产环境！**  
  
## 1. 镜像说明
> 此镜像基于 ilemonrain/lamp 作为母镜像，进行了修改以适配Cloudreve的环境要求（但我知道有些地方还是没优化到位……）。到目前为止。此镜像已经集成：  
> - 一套LAMP环境，包括：  
>   - L：Alpine Linux 3.7.0    
>   - A：Apache 2.4.29    
>   - M：MariaDB 10.1.28    
>   - P：PHP 7.1.15    
> - Cloudreve (使用Composer+Git同步官网最新版本)    
> - Google 2FA 组件 (用于支持Google两步验证)  
> - 启动过程中的日志追踪功能 (docker run -t 或者 docker logs)  
>
> 计划中 (也可能鸽掉不做) 的功能：  
> - Apache 2 SSL支持  
> - Let's Encrypt 支持 (用于部署时自动申请SSL证书)  
> - Sendfile 下载支持 (Apache2的扩展，用于减轻服务器下载压力)  
> - 一些Apache2/PHP7/MariaDB的性能优化  
>
> 目前完全是一时冲动（什么鬼）写出来的镜像，所以以后可能会坑掉不写，如果有什么好的建议或者紧急BUG提交，请Email到：ilemonrain@ilemonrain.com，谢谢你们的支持！  
  
## 2. 食用方法
>
> **个人建议在全新干净的环境下部署此镜像，可以避免很多未知问题！**    
> **如果实在没有环境，也不是不能部署……认真参考教程也可以部署成功的……有问题请发E-mail进行Bug Report……**  
>
> 启动命令行 (方括号为可选命令) ：  
> 
> docker run [-t] -p 80:80 -v /cloudreve:/cloudreve -e CLOUDREVE_URL="http://cloudreve.example.com:6780/" -e APACHE2_WEB_PORT="6780" --name ilemonrain/cloudreve ilemonrain/cloudreve
>
> -t：启动过程中进行日志追踪，可按Ctrl+C退出，使用“docker attach ilemonrain/cloudreve”命令继续追踪（但如果已经启动完成，则建议使用“docker logs ilemonrain/cloudreve”来调取启动日志）  
> -v：将目录映射到主机（强烈建议带此参数，为了数据持久化和安全性）  
> -p 80:80：暴露容器内部的80端口到容器外部（不建议修改）  
> -e CLOUDREVE_URL：Cloudreve所绑定的URL，请严格填写，前面带“http/https（以后进行支持）”，后面带“/”，参考示范见上面的启动命令行，否则会导致启动异常！  
> -e APACHE2_WEB_PORT：[BUG修复,临时增加] 指定Apache2绑定的端口，如果遇到80端口冲突，请修改此值为其他非80值，同时相应修改CLOUDREVE_URL变量的地址，默认端口暂时定为6780端口  
>   
> 更多的可配置环境参数如下：    
> Alpine Linux ROOT密码：ROOT_PASSWORD="cloudreve"    
> Apache2 默认端口：
> MySQL 数据库路径：MYSQL_URL="localhost"    
> MySQL 数据库名称：MYSQL_DATABASE="cloudreve_db"    
> MySQL 数据库端口：MYSQL_PORT="3306"    
> MySQL 用户名：MYSQL_USER="cloudreve"    
> MySQL 密码：MYSQL_PASSWORD="cloureve"     
> MySQL ROOT密码：MYSQL_ROOT_PASSWORD="cloudreve"    
> Cloudreve 绑定URL：CLOUDREVE_URL="http://localhost/"  

## 3. BUG修复记录
> - 问题: 80端口占用问题
>   - 修复方案: 加入一个控制开关 APACHE2_WEB_PORT , 并且设置默认值为 6780 , 即意味着设置的URL将会变成类似 http://cloudreve.yourdomain.com:6780/这样, 近期尝试寻找个更好的方案来解决这个问题